import { useState, useEffect, Fragment } from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Link from 'next/link'
import Router from 'next/router'
import Container from 'react-bootstrap/Container'
import { Row, Col } from 'react-bootstrap'
import NavBar from '../../components/NavBar'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import ButtonToolbar from 'react-bootstrap/ButtonToolbar'
import Card from 'react-bootstrap/Card'
import Dropdown from 'react-bootstrap/Dropdown'


import AppHelper from '../../app_helper.js'

export default function createcategory(){
	
	const [token, setToken] = useState('')
	const [category, setCategory] = useState('')
	const [categoryType, setCategoryType] = useState('Type')
	const [isActive, setIsActive] = useState(false)
	const [userId, setUserId] = useState('')

	useEffect(()=>{

		setToken(localStorage.getItem('token'))

	},[token])



	function getUserDetails(){

		const options = {

          headers: {

            Authorization: `Bearer ${token}`
          }
        }

        fetch(`${AppHelper.API_URL}/users/details`, options)
        .then(AppHelper.toJSON)
        .then(data => {

        	setUserId(data._id)
        })

	}

	getUserDetails()


	function submit(e){

		e.preventDefault()

		const payload = {

			method: "POST",
			headers: {

				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`

			},
			body: JSON.stringify({

				_id: userId,
				category: category,
				categoryType: categoryType

			})
		}

		fetch(`${AppHelper.API_URL}/users/createcategory`, payload)
		.then(AppHelper.toJSON)
		.then(data => {

			console.log(data)

		})

		Router.push('/categories')

	}

	return (
		<Fragment>
			<NavBar />
			
			 <div className="categoryContainer">
              <h1 className="text-center">Create Category</h1>
          
              <Form id="categoryForm" onSubmit={e => submit(e)}>
                <Form.Group controlId="category">
                  <Form.Label>Category Name</Form.Label>
                  <Form.Control type="text" onChange={e => setCategory(e.target.value)} placeholder="Enter Category" required/>
                </Form.Group>

                <Form.Group controlId="categoryType">
                  <Form.Label>Category Type</Form.Label>
                  
                  	<Dropdown>
					  <Dropdown.Toggle variant="dark" id="dropdown-basic">
					    {categoryType}
					  </Dropdown.Toggle>

					  <Dropdown.Menu>
					    <Dropdown.Item onClick={ e => setCategoryType('Income') }>Income</Dropdown.Item>
					    <Dropdown.Item onClick={ e => setCategoryType('Expense') }>Expense</Dropdown.Item>
					  </Dropdown.Menu>
					</Dropdown>

                </Form.Group>
                <Button variant="dark" type="submit" block>Submit</Button>
              </Form>
            </div>

            <footer className="footerBar fixed-bottom">
				© Copyright {AppHelper.dateToday.getFullYear()} 
			</footer>	
			
		</Fragment>
		/*
		*/

	)

}




