import { Fragment } from 'react'
import NavBar from '../../components/NavBar'
import Image from 'next/image'
import Jumbotron from 'react-bootstrap/Jumbotron'
import Container from 'react-bootstrap/Container'
import {Row, Col} from 'react-bootstrap'
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'
import AppHelper from '../../app_helper.js'
import Link from 'next/link'


export default function about(){


	return (

		<Fragment>
			<NavBar />
		
			<Jumbotron className="text-center jumbotron">
				<h1>We're Developers, Gamers and Tech Enthusiasts</h1>
				<Button>Work with us!</Button>
			</Jumbotron>
			<Container>
					
				<Row className="mb-5">
					
					<Col className="image1">
					
						<img src="/images/profilepic-jam.jpg" alt="jam" className="jam mb-5"  />

						<blockquote class="blockquote mb-5 text-center">
						  <p class="mb-0">"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante."</p>
						  <footer class="blockquote-footer  mt-2">Jamil Riotoc, <cite title="Source Title">Full Stack Web Developer</cite></footer>
						</blockquote>

						<p className="mb-0"><strong>Email:</strong> jamil.riotoc@gmail.com</p>
						<p className="mb-0"><strong>LinkedIn:</strong> jamil.riotoc@gmail.com</p>
						<p><strong>Contact:</strong> 0995 424 7623</p>
					
		    		</Col>
					
					
					<Col className="image2">
					
						<img src="/images/profilepic-gim.jpg" alt="gim" className="gim mb-5" />
					
						<blockquote class="blockquote mb-5 text-center">
						  <p class="mb-0">"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante."</p>
						  <footer class="blockquote-footer mt-2">Giemel Morales, <cite title="Source Title">Full Stack Web Developer</cite></footer>
						</blockquote>

						<p className="mb-0"><strong>Email:</strong> moralesgiemel@gmail.com</p>
						<p className="mb-0"><strong>LinkedIn:</strong> <Link href="https://linkedin.com/in/giemel-morales-b7b0721bb"><a target="_blank">linkedin.com/in/giemel-morales-b7b0721bb</a></Link></p>
						<p><strong>Contact:</strong> 0915 521 6760</p>
						
					</Col>

				</Row>
				
			</Container>
			<footer className="footerBar">
				© Copyright {AppHelper.dateToday.getFullYear()} 
			</footer>	
		</Fragment>
	)


}

