import { Fragment, useEffect, useState } from 'react'
import moment from 'moment' 
import AppHelper from '../../app_helper'
import NavBar from '../../components/NavBar'
import BarChart from '../../components/BarChart'
import Container from 'react-bootstrap/Container'

export default function monthlyincome(){

	const [token, setToken] = useState('')
	const [records, setRecords] = useState([])
	
	useEffect(()=>{

		setToken(AppHelper.getAccessToken())

	},[token])


	useEffect(()=>{
		
		if(token){

			const options = {

				headers: {

					Authorization: `Bearer ${token}`

				}
			}

			fetch(`${AppHelper.API_URL}/users/get-all-income`, options)
			.then(AppHelper.toJSON)
			.then(data => {

				setRecords(data)
				
			})
		}

	},[token])


	

	return (


		<Fragment>
			<NavBar />
			<Container className="mt-4">
				<BarChart records={records} />
			</Container>
			<footer className="footerBar fixed-bottom">
				© Copyright {AppHelper.dateToday.getFullYear()} 
			</footer>	
		</Fragment>

	)

}





