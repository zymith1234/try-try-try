import { useState, useEffect, Fragment } from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Link from 'next/link'
import Router from 'next/router'
import Container from 'react-bootstrap/Container'
import { Row, Col } from 'react-bootstrap'
import NavBar from '../../components/NavBar'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import ButtonToolbar from 'react-bootstrap/ButtonToolbar'
import Card from 'react-bootstrap/Card'
import Dropdown from 'react-bootstrap/Dropdown'


import AppHelper from '../../app_helper.js'

export default function createrecord(){

	const [categoryName, setCategoryName] = useState('Set Category Name')
	const [categoryType, setCategoryType] = useState('Select Category Type')
	const [amount, setAmount] = useState(0)
	const [description, setDescription] = useState('')
	const [isActive, setIsActive] = useState(false)

	const [token, setToken] = useState('')
	const [categories, setCategories] = useState([])
	const [records, setRecords] = useState([])
	const [userId, setUserId] = useState('')
	

	useEffect(()=>{

		setToken(AppHelper.getAccessToken())

	},[token])

	useEffect(()=>{

		if(categoryName !== 'Set Category Name' && categoryType !== 'Select Category Type' && amount !== 0 && description !== '')
		{
			setIsActive(true)
		}

	},[categoryName,categoryType,amount,description])


	useEffect(()=>{
		
		if(token){

			const options = {

				headers: {

					Authorization: `Bearer ${token}`

				}
			}

			fetch(`${AppHelper.API_URL}/users/details`,options)
			.then(AppHelper.toJSON)
			.then(data => {

				setCategories(data.categories)
				setRecords(data.records)
				setUserId(data._id)

			})
		}

	},[token])



	//Income and Expense Filter
	let tempIncome = []
	let tempExpense = []

	categories.forEach(element => {

			if(element.categoryType==="Income"){
				
				tempIncome.push(element.category)
			
			} else {

				tempExpense.push(element.category)
			}	
	})

	let incomeArr = tempIncome.map(income => {

		return (<Dropdown.Item onClick={ e => setCategoryName(income) }>{income}</Dropdown.Item>)
	
	})

	let expenseArr = tempExpense.map(expense => {

		return (<Dropdown.Item onClick={ e => setCategoryName(expense) }>{expense}</Dropdown.Item>)
	
	})


	/*console.log(incomeArr)
	console.log(expenseArr)
	console.log(userId)*/
	

	function submit(e){
		
		e.preventDefault()
		let tempBalance = 0
		let tempAmount = 0
		//if records is empty
		if(records.length === 0){

			//balance = amount
		   	tempBalance = parseInt(amount)

		  //add new amount to records.balance, last record
		} else {

			let indexOfLastRecord = records.length-1
			
			if(categoryType==="Income"){

				tempBalance = parseInt(records[indexOfLastRecord].balance) + parseInt(amount)
				
			} else {

				tempBalance = parseInt(records[indexOfLastRecord].balance) - parseInt(amount)
				
			}

		}
				console.log(tempBalance)
			    /*console.log(balance)*/
			    //console.log(amount)
		
		const payload = {

			method: "POST",
			headers: {
				
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({

				_id: userId,
				categoryType: categoryType,
				categoryName: categoryName, 
				amount: amount, 
				description: description,
				balance: tempBalance

			})
		}

		fetch(`${AppHelper.API_URL}/users/createrecord`, payload)
		.then(AppHelper.toJSON)
		.then(data=>{

			console.log(data)
		})

		//update to records page
		Router.push("/records")

	}



	return(

		<Fragment>
			<NavBar />
				
				<div className="recordContainer">
					<h1 className="text-center">New Record</h1>
					<Form id="recordForm" onSubmit={e => submit(e)}>
						
						<Form.Group controlId="categoryType">
							<Form.Label>Category Type</Form.Label>
							<Dropdown>
								<Dropdown.Toggle variant="dark" id="dropdown-basic">{categoryType}</Dropdown.Toggle>
								<Dropdown.Menu>
									<Dropdown.Item onClick={ e => setCategoryType('Income') }>Income</Dropdown.Item>
					    			<Dropdown.Item onClick={ e => setCategoryType('Expense') }>Expense</Dropdown.Item>
								</Dropdown.Menu>
							</Dropdown>
						</Form.Group>
						
						<Form.Group controlId="categoryType">	  
							<Form.Label>Category Name</Form.Label>
							<Dropdown>
								<Dropdown.Toggle variant="dark" id="dropdown-basic">{categoryName}</Dropdown.Toggle>
								<Dropdown.Menu>
									{
										categoryType==='Select Category Type'
										? null
										:  
											categoryType==='Income'
											? incomeArr
											: expenseArr
							
									}
								</Dropdown.Menu>
							</Dropdown>
						</Form.Group>

						<Form.Group controlId="category">
							<Form.Label>Amount</Form.Label>
							<Form.Control type="number" onChange={e => setAmount(e.target.value)} placeholder="Enter Amount" required/>
						</Form.Group>

						<Form.Group controlId="category">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" onChange={e => setDescription(e.target.value)} placeholder="Enter Description" required/>
						</Form.Group>
						{
							isActive
							? <Button variant="dark" type="submit" block>Submit</Button>
							: <Button variant="dark" block disabled>Submit</Button>
						}
					</Form>
				</div>
				
				<footer className="footerBar fixed-bottom">
					© Copyright {AppHelper.dateToday.getFullYear()} 
				</footer>
		</Fragment>			

	)

}