import { useState, useEffect } from 'react'
import NavBar from '../../components/NavBar'
import Link from 'next/link'
import Image from 'next/image'
import Carousel from 'react-bootstrap/Carousel'
import Container from 'react-bootstrap/Container'
import {Row, Col} from 'react-bootstrap'
import Card from 'react-bootstrap/Card'
import ListGroup from 'react-bootstrap/ListGroup'
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'

import AppHelper from '../../app_helper.js'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoins, faShoppingBag, faSortUp, faSortDown, faCaretUp, faCaretDown } from '@fortawesome/free-solid-svg-icons'

export default function home(){

	const [ firstName, setFirstName ] = useState("")
	const [ lastName, setLastName ] = useState("")
	const [ email, setEmail ] = useState("")
	const [ token, setToken] = useState("")
	const [ balance, setBalance] = useState(0)
	const [ totalIncome, setTotalIncome] = useState(0)
	const [ totalExpense, setTotalExpense] = useState(0)
	/*const [ averageIncome, setAverageIncome ] = useState(0)
	const [ averageExpense, setAverageExpense] = useState(0)*/
	const [ incomeRecordsLength, setIncomeRecordsLength ] = useState(0)
	const [ expenseRecordsLength, setExpenseRecordsLength ] = useState(0)

	const [ incomeArr, setIncomeArr] = useState([])
	const [ expenseArr, setExpenseArr] = useState([])

	


	const coinsIcon = <FontAwesomeIcon icon={faCoins} />
	const shoppingBagIcon = <FontAwesomeIcon icon={faShoppingBag} />
	const caretUpIcon = <FontAwesomeIcon icon={faCaretUp} color="green" />
	const caretDownIcon = <FontAwesomeIcon icon={faCaretDown} color="red" />

	useEffect(()=>{

		setToken(AppHelper.getAccessToken())

	},[])


	useEffect(()=>{

		if(token){

			const options = {

				headers: {

					"Authorization": `Bearer ${token}`
				}

			}

			fetch(`${AppHelper.API_URL}/users/details`, options)
			.then(AppHelper.toJSON)
			.then(data => {

				console.log(data)

				setFirstName(data.firstName)
				setLastName(data.lastName)
				setEmail(data.email)
				
				let lastIndex = data.records.length-1
				//change #1
				if (data.records.length != 0){
					setBalance(data.records[lastIndex].balance)
				} 
			})

			fetch(`${AppHelper.API_URL}/users/get-all-income`,options)
			.then(AppHelper.toJSON)
			.then(data => {

				let tempIncome = 0
				let tempIncomeArr = []
				data.forEach(element => {

					tempIncome += element.amount
					tempIncomeArr.push(element.amount)
				})				
				
				setIncomeRecordsLength(data.length)
				setTotalIncome(tempIncome)
				setIncomeArr(tempIncomeArr)
			})	



			fetch(`${AppHelper.API_URL}/users/get-all-expenses`,options)
			.then(AppHelper.toJSON)
			.then(data => {

				

				let tempExpense = 0
				let tempExpenseArr = []
				data.forEach(element => {

					tempExpense += element.amount
					tempExpenseArr.push(element.amount)
				})

				setExpenseRecordsLength(data.length)
				setTotalExpense(tempExpense)
				setExpenseArr(tempExpenseArr)
				
			})
		
		}

	
	},[token])
	
		console.log(totalIncome)
		console.log(totalExpense)
		console.log(incomeRecordsLength)
		console.log(expenseRecordsLength)
		console.log(incomeArr)
		console.log(expenseArr)


		let averageIncome = totalIncome/incomeRecordsLength
		let averageExpense = totalExpense/expenseRecordsLength
		let highestIncome = Math.max(...incomeArr)	
		let highestExpense = Math.max(...expenseArr)	

	
	return(
			<>
			<NavBar />
			<Container className="profileContainer">
				{/*<Image
	               src="/images/male-avatar.svg"
	                alt="Picture of the author"
	                width={50}
	                height={50}
              	/>*/}
				<h2>{`${firstName} ${lastName}`}</h2>
				<p>{`${email}`}</p>
				<Container>
					<Card className="mb-3">
						<Card.Body>
							<Card.Title>Current Balance: </Card.Title>
							<Card.Text>
	      						<h3>₱ {balance}</h3>
	      					</Card.Text>
						</Card.Body>
					</Card>
					<Card className="mb-3">
						<Card.Body>
							<Card.Title>Statistics</Card.Title>
							<Card.Text> 
	      						
	      							<Row className="mb-3">
	      								<Col xs={12}>
	      									{coinsIcon} Average Income:  ₱ {averageIncome} 
	      								</Col>
	      								<Col>
	      									{shoppingBagIcon} Average Expense: ₱ {averageExpense}
	      								</Col>
	      							</Row>
	      							<Row className="mb-3">
	      								<Col xs={12}>
	      									{caretUpIcon} Highest Income Transaction: ₱ {highestIncome}
	      								</Col>
	      								<Col>
	      									{caretDownIcon} Highest Expense Transaction: ₱ {highestExpense}
	      								</Col>
	      							</Row>
	      						
	      					</Card.Text>
						</Card.Body>
					</Card>
					<Card className="mb-3">
						<Card.Body>		
  							<Carousel>
							  <Carousel.Item>
							    <Link href="https://etoro.com" >
								    <a target="_blank">
									    <img
									      className="d-block w-100"
									      src="/images/etoro.png"
									      alt="Etoro"
									    />
								    </a>
							    </Link>
							  </Carousel.Item>
							  <Carousel.Item>
							    <Link href="https://upwork.com">
							    	<a target="_blank">
							    	   <img
									      className="d-block w-100"
									      src="/images/upwork.png"
									      alt="Upwork"
							    		/>
							    	</a>
							    </Link>
							  </Carousel.Item>
							  <Carousel.Item>
							    <Link href="https://upwork.com">
								    <a target="_blank">
									    <img
									      className="d-block w-100"
									      src="/images/shopify.png"
									      alt="Shopify"
									    />
								    </a>
							    </Link>
							  </Carousel.Item>
							</Carousel>
						</Card.Body>
					</Card>
						
					
					
				</Container>
			</Container>
			<footer className="footerBar">
				© Copyright {AppHelper.dateToday.getFullYear()} 
			</footer>	
					
			</>
		)


}