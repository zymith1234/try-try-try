import {Fragment, useState, useEffect} from 'react'

import NavBar from '../../components/NavBar'
import Button from 'react-bootstrap/Button'
import Table from 'react-bootstrap/Table'
import Container from 'react-bootstrap/Container'
import AppHelper from '../../app_helper'

import Link from 'next/link'


export default function categories(){

	const [token, setToken] = useState('')
	const [categories, setCategories] = useState([])


	useEffect(()=>{

		setToken(AppHelper.getAccessToken())

	},[token])

	useEffect(()=>{
		
		if(token){

			const options = {

				headers: {

					Authorization: `Bearer ${token}`

				}
			}

			fetch(`${AppHelper.API_URL}/users/details`,options)
			.then(AppHelper.toJSON)
			.then(data => {

				setCategories(data.categories)
			})
		}

	},[token])

	const listCategories = categories.map(element => {


		return(
			
			<tr>
				<td>{element.category}</td>  <td>{element.categoryType}</td>
			</tr>
		)	
	})

	return(
		<Fragment>
			<NavBar />
			<Container className="mt-5">
				<Button variant="success" href="/createcategory" className="mb-3">Add Category</Button>
				<Table striped bordered hover className="mb-5">
					<thead>
						<tr>
							<th>Category</th><th>Type</th>
						</tr>
						{ listCategories }
					</thead>
					<tbody>
						
					</tbody>
				</Table>

			</Container>
			<footer className="footerBar fixed-bottom">
				© Copyright {AppHelper.dateToday.getFullYear()} 
			</footer>	
		</Fragment>
	)


}