import React, {useState,useEffect} from 'react'

import Router from 'next/router'
import Image from 'next/image'

import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Nav from 'react-bootstrap/Nav'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import ButtonGroup from 'react-bootstrap/ButtonGroup'


import AppHelper from '../../app_helper.js'
import Swal from 'sweetalert2'
const results = require('../../results')

export default function register(){
	
	console.log(AppHelper)

	

	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [confirmPassword, setConfirmPassword] = useState("")
	const [isButtonActive, setIsButtonActive] = useState(false)
	const [gender, setGender] = useState("male")
	

	useEffect(()=> {

		if(firstName !== "" && lastName !== "" && email !== "" && password !== "" && confirmPassword !== ""){

			setIsButtonActive(true)

		} else {

			setIsButtonActive(false)

		}

	})

	
	function validation(e){

		e.preventDefault()

		//if password mismatch
		if(confirmPassword!==password){
			
			//Alert - The password that you entered did not match.
			//pass object key value pairs from results
			Swal.fire({

				...results.result.passwordMismatch
			
			})
		
		//else password matched
		} else {


			const payload = {

				method: 'POST',
				headers: {

					"Content-Type": "application/json"

				},
				body: JSON.stringify({

					email: email
				})

			}

			//Check if email already exists in the database - returns true if exists, otherwise false
			//if true show alert, else register the user
			fetch(`${AppHelper.API_URL}/users/email-exists`, payload)
			.then(AppHelper.toJSON)
			.then(data => {

				//true
				//email already exists, show an alert
				if(data){

					Swal.fire({

						...results.result.emailExists

					})

				//false
				} else {


					let payload = {

						method: "POST",
						headers: {
							"Content-Type": "application/json"
						},
						body: JSON.stringify({

							firstName: firstName,
							lastName: lastName,
							email: email,
							password: password
						})
					
					}

					//register the user
					//returns true if registration successful, else false if there are errors
					fetch(`${AppHelper.API_URL}/users/register`, payload)
					.then(AppHelper.toJSON)
					.then(data => {

						//true
						//show an successful alert then redirect
						if(data){

							let timerInterval
							Swal.fire({
							  title: 'Registration Successful!',
							  icon: 'success',
							  text: 'You will now be redirected to the Login Page',
							  timer: 3000,
							  timerProgressBar: true,
							  didOpen: () => {
							    Swal.showLoading()
							    timerInterval = setInterval(() => {
							      const content = Swal.getContent()
							      if (content) {
							        const b = content.querySelector('b')
							        if (b) {
							          b.textContent = Swal.getTimerLeft()
							        }
							      }
							    }, 100)
							  },
							  willClose: () => {
							    clearInterval(timerInterval)
							  }
							}).then((result) => {
							 
							  if (result.dismiss === Swal.DismissReason.timer) {
							    console.log('I was closed by the timer')
							  }
							})

							//redirect user to login page
							Router.push('/')

						//false
						//show a error alert, will not redirect
						} else {

							Swal.fire({
							 

								...results.result.error

							})
						}

					}) //end of register fetch


				
				}// end of else user email exists

			})//end of email exists fetch

		} //end of else password matched

	}

    return (
    	<>	
    		{
	    		
		        <Container className="registerContainer">
		        	
		        	<Row id="registerForm">
		        		<Form id="registerForm" className="container-fluid container-md container-lg" onSubmit={e => validation(e)}>
						
							<Container className="formContents">
								<div id="formHeader"><h1>Sign Up</h1></div>
								<Row>
									<Col >
										<Form.Group controlId="firstName">
											<Form.Control type="text" value={firstName} onChange={e => setFirstName(e.target.value)} placeholder="First Name" required/>
										</Form.Group>
									</Col>
									<Col>
										<Form.Group controlId="lastName">
											<Form.Control type="text" value={lastName} onChange={e => setLastName(e.target.value)} placeholder="Last Name" required/>
										</Form.Group>
									</Col>
								</Row>
								<Row>
									<Col>
										<Form.Group controlId="email">
											<Form.Control type="email" value={email} onChange={e => setEmail(e.target.value)} placeholder="Email" required/>
										</Form.Group>
									</Col>
								</Row>
								<Row>
									<Col>
										<Form.Group controlId="password">
											<Form.Control type="password" value={password} onChange={e => setPassword(e.target.value)} placeholder="Password" required/>
										</Form.Group>
									</Col>
									<Col>
										<Form.Group controlId="confirmPassword">
											<Form.Control type="password" value={confirmPassword} onChange={e => setConfirmPassword(e.target.value)} placeholder="Confirm password" required/>
										</Form.Group>
									</Col>
								</Row>

								{
									isButtonActive
									? <Button variant="primary" type="submit" block>Submit</Button>
									: <Button variant="secondary" type="submit" block disabled>Submit</Button>
									
								}
							<Nav.Link href="/">Already have an account?</Nav.Link>
							</Container>
						
						</Form>
		        	</Row>
		        </Container>
        	}
        </>
    )

}