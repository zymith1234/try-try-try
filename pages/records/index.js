import {Fragment, useState, useEffect} from 'react'

import NavBar from '../../components/NavBar'
import Button from 'react-bootstrap/Button'
import Table from 'react-bootstrap/Table'
import Container from 'react-bootstrap/Container'
import AppHelper from '../../app_helper'
import Card from 'react-bootstrap/Card'
import { Row, Col } from 'react-bootstrap'
import Link from 'next/link'
import Form from 'react-bootstrap/Form'
import Dropdown from 'react-bootstrap/Dropdown'
import moment from 'moment'

export default function records(){

	const [token, setToken] = useState('')
	
	//For storing inital render - all records
	const [records, setRecords ] = useState([])
	
	const [filteredRecords, setFilteredRecords] = useState([])
	const [searchInput,setSearchInput] = useState("")
	const [filter, setFilter] = useState("Select Category Type")

	//For storing rerender - all records
	const [allArray, setAllArray] = useState([])
	//For storing rerender - income records
	const [incomeArray, setIncomeArray] = useState([])
	//For storing rerender - expense records
	const [expenseArray, setExpenseArray] = useState([])




	//after initial render, get token from localStorage and store in token state
	useEffect(()=>{

		setToken(AppHelper.getAccessToken())

	},[])

	//after initial render, store all records in records state
	useEffect(()=>{
		
		if(token){

			if(filter==="Select Category Type" || filter==="All"){
				
				const options = {

					headers: {

						Authorization: `Bearer ${token}`
					}
				}

				fetch(`${AppHelper.API_URL}/users/details`,options)
				.then(AppHelper.toJSON)
				.then(data => {

					if(searchInput!==""){
					
						const searchRecords = data.records.filter(element => {

							//fix toLowerCase Jan 25
							return element.description.toLowerCase().includes(searchInput.toLowerCase())

						})	
					
						if(searchRecords.length!==0){

							setRecords(searchRecords)	
						
						} else {

							//setRecords(records.push(<h1>No Records Found</h1>))
							setRecords([])

						}	
					
					} else {

						setRecords(data.records.reverse())
					}
						
				})
		
			} else if(filter==="Income"){
				
				const options = {

					headers: {

						Authorization: `Bearer ${token}`
					}
				}

				fetch(`${AppHelper.API_URL}/users/get-all-income`,options)
				.then(AppHelper.toJSON)
				.then(data => {

					if(searchInput!==""){
					
						const searchRecords = data.filter(element => {

							//fix toLowerCase Jan 25
							return element.description.toLowerCase().includes(searchInput.toLowerCase())

						})	
					
						if(searchRecords.length!==0){

							setRecords(searchRecords)	
						
						} else {

							//setRecords(records.push(<h1>No Records Found</h1>))
							setRecords([])
						}	
					
					} else {

						setRecords(data.reverse())
					}
						
				})
		
			} else if(filter==="Expense"){
				
				const options = {

					headers: {

						Authorization: `Bearer ${token}`
					}
				}

				fetch(`${AppHelper.API_URL}/users/get-all-expenses`,options)
				.then(AppHelper.toJSON)
				.then(data => {

					if(searchInput!==""){
					
						const searchRecords = data.filter(element => {

							//fix toLowerCase Jan 25
							return element.description.toLowerCase().includes(searchInput.toLowerCase())

						})	
					
						if(searchRecords.length!==0){

							setRecords(searchRecords)	
						
						} else {

							//setRecords(records.push(<h1>No Records Found</h1>))
								setRecords([])
						}	
					
					} else {

						setRecords(data.reverse())
					}
						
				})
		
			}

		}

	},[token, searchInput,filter])

	// /console.log(records)
	

	
	const listRecords = records.map((element,index) => {
		
			return(
			
				<Row key={index}>
					<Col>
						<Card>
							<Card.Body>
								<Card.Title>
									<Row>
										<Col xs={9} md={10} lg={11} className="text-start">{element.description}</Col>
										{
											element.categoryType==="Income"
											? <Col xs={3} md={2} lg={1} className="text-center text-success"><h6>+{element.amount}</h6></Col>
											: <Col xs={3} md={2} lg={1} className="text-center text-danger"><h6>-{element.amount}</h6></Col>
										}
									</Row>
								</Card.Title>
								<Card.Text>
									<Row>
										<Col>
											<p>
												{
												   element.categoryType==="Income"
												   ? <div className="text-success inline"><strong>{element.categoryType}</strong></div>
												   : <div className="text-danger inline"><strong>{element.categoryType}</strong></div>	  
												}

												 ({element.categoryName})
											</p>	
											<p>{element.date.toLocaleString()}</p>
										</Col>
										
										{

											element.categoryType==="Income"
											? <Col xs={3} md={2} lg={1} className="text-center text-success"><h5>{element.balance}</h5></Col>
											: <Col xs={3} md={2} lg={1} className="text-center text-danger"><h5>{element.balance}</h5></Col>
										}
										
									</Row>

									
								</Card.Text>
							</Card.Body>
						</Card>
					</Col>
				</Row>
			)	
		})
	




	function submit(e){

		e.preventDefault()		
		
	}

	function displayCategory(e, category){

		e.preventDefault()
		//setSearchInput(e.target.value)

		if(category==="Select Category Type"){

			const options = {

				headers: {

					Authorization: `Bearer ${token}`
				}
			}

			fetch(`${AppHelper.API_URL}/users/get-all`,options)
			.then(AppHelper.toJSON)
			.then(data => {

				//""
				//all

				//filter records with pattern inside searchInput
					//temp = filtered data
					//data = temp

				if(searchInput!==""){
					
					const searchRecords = data.filter(element => {

						//fix toLowerCase Jan 25
						return element.description.toLowerCase().includes(searchInput.toLowerCase())

					})	
				
					setRecords(searchRecords)	
				}

				else{

					setRecords(data.reverse())
				}
			
			})

		} else if(category==="All"){

			setFilter("All")

			const options = {

				headers: {

					Authorization: `Bearer ${token}`
				}
			}

			fetch(`${AppHelper.API_URL}/users/get-all`,options)
			.then(AppHelper.toJSON)
			.then(data => {

				//""
				//all

				//filter records with pattern inside searchInput
					//temp = filtered data
					//data = temp

				if(searchInput!==""){
					
					const searchRecords = data.filter(element => {

						//fix toLowerCase Jan 25
						return element.description.toLowerCase().includes(searchInput.toLowerCase())

					})	
				
					setRecords(searchRecords)	
				}

				else{

					setRecords(data.reverse())
				}
			
			})

		} else if(category==="Income"){

			setFilter("Income")

			const options = {

				headers: {

					Authorization: `Bearer ${token}`
				}
			}

			fetch(`${AppHelper.API_URL}/users/get-all-income`,options)
			.then(AppHelper.toJSON)
			.then(data => {

				setRecords(data.reverse())

			})

		} else if(category==="Expense") {


			setFilter("Expense")

			const options = {

				headers: {

					Authorization: `Bearer ${token}`
				}
			}

			fetch(`${AppHelper.API_URL}/users/get-all-expenses`,options)
			.then(AppHelper.toJSON)
			.then(data => {

				setRecords(data.reverse())
				
			})
		}

	}
	

	return(

		<Fragment>
			<NavBar />
			<Container className="listRecordsContainer">
				
				<Button variant="success" href="/createrecord" className="mt-4">Add Record</Button>
				
				<Form onSubmit={ e => submit(e) }>
					
					<Form.Control type="text" placeholder="Search" onChange={ e => setSearchInput(e.target.value) } className="mt-3 mb-3" />
				
					<Form.Group controlId="categoryType">
	                	
	                  	<Dropdown>
						  <Dropdown.Toggle variant="dark" id="dropdown-basic">
						    {filter}
						  </Dropdown.Toggle>
						  <Dropdown.Menu>
						    <Dropdown.Item onClick={ e => displayCategory(e, "All") }>All</Dropdown.Item>
						    <Dropdown.Item onClick={ e => displayCategory(e, "Income") }>Income</Dropdown.Item>
						    <Dropdown.Item onClick={ e => displayCategory(e, "Expense") }>Expense</Dropdown.Item>
						  </Dropdown.Menu>
						</Dropdown>
	                </Form.Group>

                </Form>
				{
					
					listRecords.length === 0
					? <h1>No records Found</h1>
					: listRecords
				}
			</Container>
			<div className="whitespace">
				
			</div>
			<footer className="footerBar fixed-bottom">
				© Copyright {AppHelper.dateToday.getFullYear()} 
			</footer>	
		</Fragment>
	)

}
