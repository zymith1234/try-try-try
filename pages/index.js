import { Fragment, useState} from 'react'
import Image from 'next/image'
import Router from 'next/router'

import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/Login.module.css'

import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Nav from 'react-bootstrap/Nav'

import Swal from 'sweetalert2'

import AppHelper from '../app_helper.js'
const results = require('../results')

import NavBar from '../components/NavBar'


export default function login(){
 
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  
  console.log(AppHelper)

  function validate(e){
    
    e.preventDefault()
    
    const payload = {

      method: "POST",
      headers: {

        "Content-Type": "application/json"

      },
      body: JSON.stringify({

        email: email,
        password: password
      
      })

    }

    //verify if user is registered
    //if true redirect to profile, else show alert
    fetch(`${AppHelper.API_URL}/users/login`, payload)
    .then(AppHelper.toJSON)
    .then(data => {
      
      if(data){
        
        localStorage.setItem('token', data.accessToken)
        
        const token = localStorage.getItem('token')
        


        const options = {

          headers: {

            Authorization: `Bearer ${token}`

          }

        }
       
        Swal.fire({ ...results.result.loginSuccess })
        Router.push('/home')

      } else {

        Swal.fire({ ...results.result.incorrect })

      } 

    }) //End of users/login fetch
    
  }

  return (
           
    <Fragment>
      <NavBar />
      
  
     
          <div className="loginContainer">
            <h1 className="text-center">Budget Tracker</h1>
            <Image
             src="/images/budget-icon.png"
              alt="Picture of the author"
              width={50}
              height={50}
            />
            <Form id="loginForm" onSubmit={e => validate(e)}>
              <Form.Group controlId="userEmail">
                <Form.Label>Email</Form.Label>
                <Form.Control type="email" onChange={e => setEmail(e.target.value)} placeholder="Enter Email" required/>
              </Form.Group>

              <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" onChange={e => setPassword(e.target.value)} placeholder="Enter Password" />
              </Form.Group>
              <Button variant="dark" type="submit" block>Login</Button>
              <div className="center-links">
                <Nav.Link href="register">Sign Up</Nav.Link> <Nav.Link href="#login">Forgot password?</Nav.Link> 
              </div>
            </Form>
          </div>
  
      
      <footer className="footerBar fixed-bottom">
        © Copyright {AppHelper.dateToday.getFullYear()} 
      </footer> 
    </Fragment>
         
  
  )


}
