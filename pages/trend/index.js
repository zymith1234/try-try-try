import { Fragment, useEffect, useState } from 'react'

import moment from 'moment' 
import AppHelper from '../../app_helper'
import NavBar from '../../components/NavBar'
import BarChart from '../../components/BarChart'
import LineChart from '../../components/LineChart'
import Container from 'react-bootstrap/Container'


export default function trend(){

	const [token, setToken] = useState('')
	const [incomeRecords, setIncomeRecords] = useState([])
	const [expenseRecords, setExpenseRecords] = useState([])
	
	useEffect(()=>{

		setToken(AppHelper.getAccessToken())

	},[])

	useEffect(()=>{
		
		if(token){

			const options = {

				headers: {

					Authorization: `Bearer ${token}`

				}
			}

			fetch(`${AppHelper.API_URL}/users/get-all-income`,options)
			.then(AppHelper.toJSON)
			.then(data => {

				setIncomeRecords(data)
				
			})	

			fetch(`${AppHelper.API_URL}/users/get-all-expenses`,options)
			.then(AppHelper.toJSON)
			.then(data => {

				setExpenseRecords(data)
				
			})

		}

	},[token])

	return (


			<Fragment>
				<NavBar />
				<Container className="mt-4">
					<LineChart incomeRecords={incomeRecords} expenseRecords={expenseRecords} />
				</Container>
				<footer className="footerBar fixed-bottom">
					© Copyright {AppHelper.dateToday.getFullYear()} 
				</footer>	
			</Fragment>


	)

}		

//{expenseRecords}
//<Line incomeRecords={incomeRecords} expenseRecords={expenseRecords} />