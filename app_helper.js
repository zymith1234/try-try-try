//process.env allows us to access the environment variable to be used in a file.


module.exports = {

	API_URL: "https://glacial-ridge-22367.herokuapp.com/api",
	getAccessToken: () => localStorage.getItem('token'),
	toJSON: res => res.json(),
	dateToday: new Date()
	
}